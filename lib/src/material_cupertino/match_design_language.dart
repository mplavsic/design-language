import 'design_language.dart';

T matchDesignLanguage<T>({
  required T Function() material,
  required T Function() cupertino,
}) {
  final designLanguage = DesignLanguage.current;
  if (designLanguage == DesignLanguages.material) {
    return material();
  } else {
    return cupertino();
  }
}
