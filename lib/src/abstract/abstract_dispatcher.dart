import 'package:flutter/widgets.dart';

abstract class AbstractDispatcher extends StatelessWidget {
  const AbstractDispatcher({
    super.key,
    required this.child,
  });

  final Widget? child;
}
