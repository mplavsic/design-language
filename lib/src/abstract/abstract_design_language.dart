abstract class AbstractDesignLanguage {
  final String name;

  const AbstractDesignLanguage(this.name);

  /// Useful when saving the used design language to persistent storage.
  @override
  String toString() => name;

  /// When changing state it is important that the name of the selected
  /// platform differs from the old state's one.
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AbstractDesignLanguage &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;
}
