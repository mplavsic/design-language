import 'package:flutter/widgets.dart';

import 'abstract_design_language.dart';

abstract class AbstractDesignLanguageNotifier<T extends AbstractDesignLanguage>
    extends ValueNotifier<T> {
  AbstractDesignLanguageNotifier(T init) : super(init);

  set chosenDesignLanguage(T designLanguage) {
    value = designLanguage;
    notifyListeners();
  }
}
