import 'package:flutter/widgets.dart';

import '../abstract/abstract_dispatcher.dart';
import 'design_language.dart';

class DesignLanguageDispatcher extends AbstractDispatcher {
  const DesignLanguageDispatcher({
    super.key,
    super.child,
    required this.cupertino,
    required this.macos,
    required this.material,
    required this.fluent,
  });

  final TransitionBuilder cupertino;
  final TransitionBuilder macos;
  final TransitionBuilder material;
  final TransitionBuilder fluent;

  @override
  Widget build(BuildContext context) => ValueListenableBuilder<DesignLanguage>(
        valueListenable: DesignLanguage.notifier,
        child: child,
        builder: (context, designLanguage, child) {
          if (designLanguage == DesignLanguages.material) {
            return material(context, child);
          } else if (designLanguage == DesignLanguages.cupertino) {
            return cupertino(context, child);
          } else if (designLanguage == DesignLanguages.macos) {
            return macos(context, child);
          } else {
            return fluent(context, child);
          }
        },
      );
}
