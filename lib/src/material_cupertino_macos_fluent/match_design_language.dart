import 'design_language.dart';

T matchDesignLanguage<T>({
  required T Function() material,
  required T Function() cupertino,
  required T Function() macos,
  required T Function() fluent,
}) {
  final designLanguage = DesignLanguage.current;
  if (designLanguage == DesignLanguages.material) {
    return material();
  } else if (designLanguage == DesignLanguages.cupertino) {
    return cupertino();
  } else if (designLanguage == DesignLanguages.macos) {
    return macos();
  } else {
    return fluent();
  }
}
