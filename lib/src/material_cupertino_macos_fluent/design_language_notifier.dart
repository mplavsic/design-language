import '../abstract/abstract_design_language_notifier.dart';
import 'design_language.dart';

class DesignLanguageNotifier
    extends AbstractDesignLanguageNotifier<DesignLanguage> {
  DesignLanguageNotifier(DesignLanguage init) : super(init);
}
