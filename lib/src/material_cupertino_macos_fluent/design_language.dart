import '../abstract/abstract_design_language.dart';
import '../utils/physical_platform_as_string.dart';
import 'design_language_notifier.dart';

class DesignLanguage extends AbstractDesignLanguage {
  const DesignLanguage._(super.name);

  /// This notifier must be instantiated before it is listened to.
  static late final DesignLanguageNotifier notifier;

  /// Useful when saving the newly selected design language to persistent
  /// storage.
  ///
  /// NB: [notifier] needs to be initialized first.
  static DesignLanguage get current => notifier.value;

  /// Specifies some overrides in case the getter [fromPhysicalPlatform]'s
  /// platform dispatcher logic needs to be partially different.
  static Map<String, DesignLanguage>? overrides;

  /// The design language that the actual platform is supposed to default to.
  static DesignLanguage get fromPhysicalPlatform {
    switch (physicalPlatformAsString) {
      case "android":
        if (overrides != null && overrides!.containsKey("android")) {
          return overrides!["android"]!;
        }
        return DesignLanguages.material;
      case "ios":
        if (overrides != null && overrides!.containsKey("ios")) {
          return overrides!["ios"]!;
        }
        return DesignLanguages.cupertino;
      case "linux":
        if (overrides != null && overrides!.containsKey("linux")) {
          return overrides!["linux"]!;
        }
        return DesignLanguages.material;
      case "macos":
        if (overrides != null && overrides!.containsKey("macos")) {
          return overrides!["macos"]!;
        }
        return DesignLanguages.macos;
      case "windows":
        if (overrides != null && overrides!.containsKey("windows")) {
          return overrides!["windows"]!;
        }
        return DesignLanguages.fluent;
      case "web":
        if (overrides != null && overrides!.containsKey("web")) {
          return overrides!["web"]!;
        }
        return DesignLanguages.material;
      case "fuchsia":
        if (overrides != null && overrides!.containsKey("fuchsia")) {
          return overrides!["fuchsia"]!;
        }
        return DesignLanguages.material;
      default:
        return DesignLanguages.material;
    }
  }

  /// Useful when loading the last used design language from persistent
  /// storage.
  ///
  /// It will default to the physical-platform-inferred design language if the
  /// [designLanguageName] is invalid.
  static DesignLanguage fromString(String designLanguageName) {
    switch (designLanguageName) {
      case "material":
        return DesignLanguages.material;
      case "cupertino":
        return DesignLanguages.cupertino;
      case "macos":
        return DesignLanguages.macos;
      default: // in case the value loaded from shared preferences is invalid
        return fromPhysicalPlatform;
    }
  }
}

/// All possible design languages.
class DesignLanguages {
  const DesignLanguages._();
  static const material = DesignLanguage._('material');
  static const cupertino = DesignLanguage._('cupertino');
  static const macos = DesignLanguage._('macos');
  static const fluent = DesignLanguage._('fluent');
}
