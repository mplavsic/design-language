import 'dart:io';

import 'package:flutter/foundation.dart';

String get physicalPlatformAsString =>
    kIsWeb ? 'web' : Platform.operatingSystem;
