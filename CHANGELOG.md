## 0.0.3

Fix broken link

## 0.0.2

Improve documentation

## 0.0.1

The first release includes:

- 3 design-language groups:

    - Material and Cupertino
    - Material, Cupertino and Macos
    - Material, Cupertino, Macos and Fluent

- the following utils, per design language:

    - DesignLanguage
    - DesignLanguages
    - DesignLanguageNotifier
    - DesignLanguageDispatcher
    - matchDesignLanguage
    - "hidden" library exporting all utils at once

- accurate documentation
