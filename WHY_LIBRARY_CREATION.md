## Why was the library creation approach taken?

It is necessary to create a library to prevent exposing too much code that can only get in the way. If there is only one library exposed, the user can be assured that every time the IDE autocompletion assistant suggests something, only the utils  (`DesignLanguageDispatcher`, `matchDesignLanguage`, ...) for the intended combination of design languages will be displayed.

### Opposite case

Imagine the opposite case (the library creation step is not necessary, i.e., all instruments can be used):

Pre-made libraries:
- `package:design_language/material_cupertino.dart`
- `package:design_language/material_cupertino_macos.dart`
- `package:design_language/material_cupertino_macos_fluent.dart`

#### Utils having the same names

Input: `DesignLanguageDi`

Suggested options:
  - `DesignLanguageDispatcher` from `package:design_language/material_cupertino.dart`
  - `DesignLanguageDispatcher` from `package:design_language/material_cupertino_macos.dart`
  - `DesignLanguageDispatcher` from `package:design_language/material_cupertino_macos_fluent.dart`

If you choose the wrong one, then there will be a group mismatch. If you don't notice it, then there will be a runtime crash, because the other group's notifier was not instantiated. Thus, this option isn't satisfying.

#### Utils having different names

Input: `MC`

Suggested options:
  - `MCDispatcher` from `package:design_language/material_cupertino.dart`
  - `MCMDispatcher` from `package:design_language/material_cupertino_macos.dart`
  - `MCMFDispatcher` from `package:design_language/material_cupertino_macos_fluent.dart`

Now would be slightly harder to make a mistake, however, there is no need to display `MCDispatcher` and `MCMFDispatcher` if the app is supposed to support exactly three design languages (i.e., the app must support all of these widgets `MaterialApp`, `CupertinoApp`, `MacosApp`, and must not support `Fluent`).

Let's say we fix the notifier problem described above. There would still be issues with this approach:

- `MCDispatcher`: a `macos` chain is missing everywhere. If the device uses `MacosApp` in the widget three, there will be a crash.
- `MCMFDispatcher`: a `fluent` chain is needed too, however, it just can't be reached.

Thus, assigning different names also isn't satisfying.

### Design language deprecation or addition

Another reason for favoring this self-declared library creation step is that if you simply change the export, e.g., from

```dart
library design_language;
export 'package:design_language/src/material_cupertino_macos/library.dart';
```

to:

```dart
library design_language;
export 'package:design_language/src/material_cupertino/library.dart';
```

then no imports will need to be changed in the rest of your application, i.e., they will all stay the same, e.g.:

```dart
import 'package:example_app/core/design/design_language.dart';
```

and at the same time, the IDE will show you errors and force you to remove or add all affected design language callbacks (in the case above, all `macos` callbacks will have to be removed).
