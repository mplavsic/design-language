import 'package:example/core/design/design_language.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  DesignLanguage.notifier =
      DesignLanguageNotifier(DesignLanguage.fromPhysicalPlatform);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DesignLanguageDispatcher(
      cupertino: (context, child) {
        return const MyCupertinoPage();
      },
      material: (context, child) {
        return const MyMaterialPage();
      },
    );
  }
}

class MyCupertinoPage extends StatelessWidget {
  const MyCupertinoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Cupertino Design demo',
      home: CupertinoPageScaffold(
        child: Center(
          child: CupertinoButton(
              child: const Text('Switch to Material'),
              onPressed: () => DesignLanguage.notifier.chosenDesignLanguage =
                  DesignLanguages.material),
        ),
      ),
    );
  }
}

class MyMaterialPage extends StatelessWidget {
  const MyMaterialPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material Design demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: ElevatedButton(
              child: const Text('Switch to Cupertino'),
              onPressed: () => DesignLanguage.notifier.chosenDesignLanguage =
                  DesignLanguages.cupertino),
        ),
      ),
    );
  }
}
